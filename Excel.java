import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.DataFormat;
import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.io.FileOutputStream;
import java.awt.Desktop;

public class Excel {
    public static HashMap<Integer, Patient> readMasterList(File masterFile){
        //Read master list excel file
        HashMap<Integer, Patient> hashedMaster = new HashMap<Integer, Patient>();
        try {
            Workbook workbook = new HSSFWorkbook(new FileInputStream(masterFile));
            Sheet sheet = workbook.getSheetAt(0);
            for (int i = 3; i < sheet.getLastRowNum() + 1; i++){
                Row row = sheet.getRow(i);
                Cell dr = row.getCell(0);
                Cell id = row.getCell(1);
                Cell ptName = row.getCell(2);
                Cell apptType = row.getCell(3);
                if (apptType == null){
                   apptType = row.getCell(5, Row.CREATE_NULL_AS_BLANK);
                }
                dr.setCellType(Cell.CELL_TYPE_STRING);
                id.setCellType(Cell.CELL_TYPE_NUMERIC);
                ptName.setCellType(Cell.CELL_TYPE_STRING);
                if (apptType != null){
                    apptType.setCellType(Cell.CELL_TYPE_STRING);
                }
                String drName = dr.getStringCellValue();
                int account = (int)id.getNumericCellValue();
                String name = ptName.getStringCellValue();
                String type = "";
                if (apptType != null){
                    type = apptType.getStringCellValue();
                }
                Patient patient = new Patient(drName, account, name, type);
                hashedMaster.put(account, patient);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Invalid master file, please try again.", "Attention", JOptionPane.ERROR_MESSAGE);
        }
        return hashedMaster;
    }

    public static HashSet<Integer> readSignedList(File signedFile){
        //Read signed arbitration list excel file
        HashSet<Integer> signedList = new HashSet<Integer>();
        try {
            Workbook workbook = new HSSFWorkbook(new FileInputStream(signedFile));
            Sheet sheet = workbook.getSheetAt(0);
            for (int i = 3; i < sheet.getLastRowNum() + 1; i++){
                Row row = sheet.getRow(i);
                Cell cell = row.getCell(0);
                cell.setCellType(0);
                int account = (int)cell.getNumericCellValue();
                signedList.add(account);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Invalid signed list, please try again.", "Attention", JOptionPane.ERROR_MESSAGE);
        }
        return signedList;
    }

    public static void saveToExcel(ArrayList<Patient> finalList){
        //Create final list in excel format
        Workbook wb = new HSSFWorkbook();
        Sheet sheet1 = wb.createSheet("Sheet1");

        Row secondRow = sheet1.createRow(1);
        Cell cellA = secondRow.createCell(0);

        CellStyle titleCellStyle = wb.createCellStyle();
        titleCellStyle.setAlignment((short)2);
        titleCellStyle.setVerticalAlignment((short)1);
        cellA.setCellStyle(titleCellStyle);

        Font titleFont = wb.createFont();
        titleFont.setBoldweight((short)700);
        titleFont.setFontHeightInPoints((short)16);
        titleFont.setFontName("Calibri");
        titleCellStyle.setFont(titleFont);

        cellA.setCellValue("PLEASE OBTAIN ARBITRATION / RELEASE OF INFORMATION");

        sheet1.addMergedRegion(new CellRangeAddress(1, 1, 0, 7));

        Row thirdRow = sheet1.createRow(3);
        Cell date = thirdRow.createCell(1);
        Cell time = thirdRow.createCell(2);
        Cell dr = thirdRow.createCell(3);
        Cell acct = thirdRow.createCell(4);
        Cell name = thirdRow.createCell(5);
        Cell type = thirdRow.createCell(6);

        CellStyle headerStyle = wb.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        headerStyle.setFillPattern((short)1);
        headerStyle.setAlignment((short)2);
        headerStyle.setVerticalAlignment((short)1);

        Font headerFont = wb.createFont();
        headerFont.setBoldweight((short)700);
        headerFont.setFontHeightInPoints((short)10.5);
        headerFont.setFontName("Calibri-Regular");
        headerStyle.setFont(headerFont);

        date.setCellStyle(headerStyle);
        time.setCellStyle(headerStyle);
        dr.setCellStyle(headerStyle);
        acct.setCellStyle(headerStyle);
        name.setCellStyle(headerStyle);
        type.setCellStyle(headerStyle);

        date.setCellValue("Date");
        time.setCellValue("Time");
        dr.setCellValue("Dr");
        acct.setCellValue("Acct #");
        name.setCellValue("Name");
        type.setCellValue("Type");

        Date currentDate = new Date();

        CellStyle dateStyle = wb.createCellStyle();
        dateStyle.setAlignment((short)2);
        dateStyle.setAlignment((short)2);
        DataFormat format = wb.createDataFormat();
        dateStyle.setDataFormat(format.getFormat("mm/dd/yy"));

        CellStyle cells = wb.createCellStyle();
        cells.setAlignment((short)2);
        cells.setAlignment((short)2);

        CellStyle ptName = wb.createCellStyle();

        Font cellFont = wb.createFont();
        cellFont.setFontHeightInPoints((short)10.5);
        cellFont.setFontName("Calibri-Regular");
        cells.setFont(cellFont);
        dateStyle.setFont(cellFont);
        ptName.setFont(cellFont);
        for (int i = 0; i < finalList.size(); i++)
        {
            Row row = sheet1.createRow(i + 4);
            Cell cellDate = row.createCell(1);
            Cell cellC = row.createCell(2);
            Cell cellD = row.createCell(3);
            Cell cellE = row.createCell(4);
            Cell cellF = row.createCell(5);
            Cell cellG = row.createCell(6);

            cellDate.setCellStyle(dateStyle);
            cellC.setCellStyle(cells);
            cellD.setCellStyle(cells);
            cellE.setCellStyle(cells);
            cellF.setCellStyle(ptName);
            cellG.setCellStyle(cells);

            cellDate.setCellValue(currentDate);
            cellC.setCellValue(finalList.get(i).getTime());
            cellD.setCellValue(finalList.get(i).getDoctorName());
            cellE.setCellValue(finalList.get(i).getId());
            cellF.setCellValue(finalList.get(i).getName());
            cellG.setCellValue(finalList.get(i).getApptType());
        }
        for (int i = 0; i < 8; i++) {
            sheet1.autoSizeColumn(i);
        }
        try
        {
            //Save excel file to desired location selected by user
            JFrame saveFrame = new JFrame();
            JFileChooser fileChooser = new JFileChooser();

            int userSelection = fileChooser.showSaveDialog(saveFrame);
            if (userSelection == 0)
            {
                File fileToSave = fileChooser.getSelectedFile();
                FileOutputStream output = new FileOutputStream(fileToSave + ".xls");
                wb.write(output);
                output.close();
                if (Desktop.isDesktopSupported())
                {
                    //Open created excel file
                    File openFile = new File(fileToSave.getAbsolutePath() + ".xls");
                    Desktop.getDesktop().open(openFile);
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

   public static void saveMasterList(Map<Integer, Patient> sortedMasterFile, File masterFile){
       //Save sorted excel master list
       Workbook workbook = new HSSFWorkbook();
       Sheet sheet = workbook.createSheet("Sheet1");

       Row rowOne = sheet.createRow(0);
       Cell cellOneA = rowOne.createCell(0);
       Row rowTwo = sheet.createRow(1);
       Cell cellTwoA = rowTwo.createCell(0);
       Row rowThree = sheet.createRow(2);
       Cell cellThreeA = rowThree.createCell(0);
       Cell cellThreeB = rowThree.createCell(1);
       Cell cellThreeC = rowThree.createCell(2);
       Cell cellThreeD = rowThree.createCell(3);

       Font font = workbook.createFont();
       font.setFontHeightInPoints((short)10.5);
       font.setFontName("Calibri-Regular");

       CellStyle style = workbook.createCellStyle();
       style.setAlignment((short)2);
       style.setVerticalAlignment((short)1);
       style.setFont(font);

       CellStyle nameStyle = workbook.createCellStyle();
       nameStyle.setFont(font);

       cellThreeA.setCellStyle(style);
       cellThreeB.setCellStyle(style);
       cellThreeC.setCellStyle(style);
       cellThreeD.setCellStyle(style);

       cellOneA.setCellValue("SANTA MONICA ORTHOPAEDIC GROUP");
       cellTwoA.setCellValue("From 10-15-12 to Current");
       cellThreeA.setCellValue("Dr");
       cellThreeB.setCellValue("Acct #");
       cellThreeC.setCellValue("Patient Name");
       cellThreeD.setCellValue("Typ");

       int i = 3;
       for (Map.Entry<Integer, Patient> entry : sortedMasterFile.entrySet()){
           Row row = sheet.createRow(i);
           Cell dr = row.createCell(0);
           Cell acct = row.createCell(1);
           Cell name = row.createCell(2);
           Cell type = row.createCell(3);

           dr.setCellStyle(style);
           acct.setCellStyle(style);
           name.setCellStyle(nameStyle);
           type.setCellStyle(style);

           dr.setCellValue(entry.getValue().getDoctorName());
           acct.setCellValue(entry.getKey());
           name.setCellValue(entry.getValue().getName());
           type.setCellValue(entry.getValue().getApptType());
           i++;
       }

       for (int j = 1; j < 4; j++){
           sheet.autoSizeColumn(j);
       }

       try {
           FileOutputStream output = new FileOutputStream(masterFile.getAbsolutePath());
           workbook.write(output);
           output.close();
       } catch (Exception e){
           e.printStackTrace();
       }
   }
}
