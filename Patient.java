import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Patient
{
    private int id;
    private String name;
    private SimpleDateFormat stf = new SimpleDateFormat("hh:mm aa", Locale.US);
    private Date time;
    private String doctorName;
    private SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
    private Date lastDOS;
    private String apptType;

    public Patient(String time, String doctorName, String id, String name, String apptType, String lastDOS)
            throws ParseException
    {
        this.id = Integer.parseInt(id);
        this.name = name;
        this.time = this.stf.parse(time);
        this.doctorName = doctorName;
        if (lastDOS == " ") {
            this.lastDOS = null;
        } else {
            this.lastDOS = this.sdf.parse(lastDOS);
        }
        this.apptType = apptType;
    }

    public Patient(String drCode, int id, String name, String apptType){
        this.doctorName = drCode;
        this.id = id;
        this.name = name;
        this.apptType = apptType;
    }


    public int getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getTime()
    {
        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa", Locale.US);
        return timeFormat.format(this.time);
    }

    public String getDoctorName()
    {
        return this.doctorName;
    }

    public String getLastDOS()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
        if (this.lastDOS == null) {
            return " ";
        }
        return dateFormat.format(this.lastDOS);
    }

    public String getApptType()
    {
        return this.apptType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patient patient = (Patient) o;

        if (id != patient.id) return false;
        if (!name.equals(patient.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        return result;
    }

    public int compareTo(Patient value) {
        return this.getName().compareTo(value.getName());
    }
}
